// create function logReceipt that accepts menu items (1 or many) as objects
// with these properties: {descr, price}
// i.e. {descr: 'Coke', price: 1.99}
// function should log each item to the console and log a total price

function logReceipt(...args) {

  let total = 0;
  let itemCount = 0;

  args.forEach((item) => {
    total = total+item.price;
    itemCount++;
    console.log(`${item.descr} - ${item.price}`);
  });

  console.log(`Subtotal (${itemCount} items) - $${total.toFixed(2)}`);

  total = total+(total*.06);
  console.log(`Total with tax (6%) (${itemCount} items) - ${total.toFixed(2)}`);
};


// Check
logReceipt(
  { descr: 'Burrito', price: 5.99 },
  { descr: 'Chips & Salsa', price: 2.99 },
  { descr: 'Sprite', price: 1.99 },
  { descr:'Bud Light', price: 3.99},
  { descr:'Hamburger', price:6.99}
);
// should log something like:
// Burrito - $5.99
// Chips & Salsa - $2.99
// Sprite - $1.99
// Total - $10.97
